const diceContainer = new DiceContainer();

const rollButton = document.getElementById('roll-button');
const resetButton = document.getElementById('reset-button');

const paintContainer = () => {
  diceContainer.rollDice()
  const container = diceContainer.getCombination();

  const containerDiv = document.getElementById('dice-container');
  containerDiv.innerHTML = ''

  container.map((diceValue) => {
    const diceElement = document.createElement('div').appendChild(document.createTextNode(diceValue))
    containerDiv.appendChild(diceElement)
  })
}

rollButton.addEventListener('click', paintContainer);
resetButton.addEventListener('click', () => diceContainer.resetRolls());