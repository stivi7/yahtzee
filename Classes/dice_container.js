class DiceContainer {
  dice = new Dice();

  numberOfDice = 5;
  rolls = 3;
  keptDice = [];
  diceHolder = [];

  rollDice() {
    if (this.rolls > 0) {
      for (let i = 0; i < this.numberOfDice; i ++) {
        if (!this.diceHolder.length) {
          this.diceHolder.push(this.dice.roll());
        } else if (!this.keptDice.includes(i)) {
          this.diceHolder[i] = this.dice.roll();
        }
      }

      this.rolls -= 1;
    }
  }

  manageDice(diceIndex) {
    const indexInKept = this.keptDice.indexOf(diceIndex);

    if (indexInKept === -1) {
      this.keptDice.push(diceIndex);
    } else {
      this.keptDice.splice(diceIndex, 1);
    }
  }

  resetRolls() {
    this.rolls = 3;
  }

  getCombination() {
    return this.diceHolder;
  }
}