class Dice {
  sides = 6

  roll() {
    return Math.ceil(Math.random() * this.sides)
  }
}
