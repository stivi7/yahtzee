class Sheet {
  upperSection = {
    ones: null,
    twos: null,
    threes: null,
    fours: null,
    fives: null,
    sixes: null,
    bonus: null,
  }

  lowerSection = {
    threeOfAKind: null,
    fourOfAKind: null,
    fullHouse: null,
    smallStraight: null,
    largeStraight: null,
    yahtzee: null,
    chance: null,
  }

  writeDownPoints(rolls, section, combination) {
    if (this[section][combination] !== null) {
      console.log('Already filled');
      return
    }

    switch (section) {
      case 'upperSection':
        this[section][combination] = this.getPointsForUpperSectionCombination(rolls, combination);
        this.setBonusPoints();
        break;
      case 'lowerSection':
        this[section][combination] = this.getPointsForLowerSectionCombination(rolls, combination);
        break;
      default:
        throw new Error('Invalid section');
    }
  }

  getTotalPoints() {
    return this.getSectionTotal('upperSection') + this.getSectionTotal('lowerSection');
  }

  getSectionTotal(section, withoutBonus) {
    const values = Object.values(this[section]);
    const length = withoutBonus ? values.length - 1 : values.length;

    let sum = 0;

    for (let i = 0; i < length; i ++) {
      if (values[i] !== null) {
        sum += values[i];
      }
    }

    return sum;
  }

  validateCombination(rolls, combination) {
    const accumulator = {};

    for (let i = 0; i < rolls.length; i ++) {
      if (!accumulator[rolls[i]]) {
        accumulator[rolls[i]] = 1;
      } else {
        const currValue = accumulator[rolls[i]];
        accumulator[rolls[i]] = currValue + 1;        
      }
    }

    // Get the number of entries
    const values = Object.values(accumulator);

    const isThreeOfAKind = values.length === 3 && values.includes(3);
    const isFourOfAKind = values.length === 2 && values.includes(4);
    const isFullHouse = values.length === 2 && values.includes(3);
    const isSmallStraight = values.length === 4 && values.includes(2);
    const isLargeStraight = values.length === 5 && values.includes(1);
    const isYahtzee = values.length === 1 && values.includes(5);
    
    switch (combination) {
      case 'fullHouse':
        return isFullHouse;
      case 'fourOfAKind':
        return isFourOfAKind || isYahtzee;
      case 'threeOfAKind':
        return  isThreeOfAKind || isFourOfAKind || isYahtzee || isFullHouse;
      case 'smallStraight':
        return isSmallStraight || isLargeStraight;
      case 'largeStraight':
        return isLargeStraight;
      case 'yahtzee':
        return isYahtzee;
      default:
        throw new Error('Provide combination');
    }
  }

  getCombinationSum(rolls) {
    let sum = 0;

    for (let i = 0; i < rolls.length; i ++) {
      sum += rolls[i];
    }

    return sum;
  }

  getPredicatedSum(rolls, predicate) {
    let points = 0;

    for (let i = 0; i < rolls.length; i ++) {
      if (rolls[i] === predicate) {
        points += rolls[i]
      }
    }

    return points;
  }

  setBonusPoints() {
    const hasAchievedBonus = this.getSectionTotal('upperSection', true) >= 63;
 
    if (hasAchievedBonus && !this.upperSection.bonus) {
      this.upperSection.bonus = 35;
    }
  }

  getPointsForUpperSectionCombination(rolls, combination) {
    switch (combination) {
      case 'ones':
        return this.getPredicatedSum(rolls, 1);
      case 'twos':
        return this.getPredicatedSum(rolls, 2);
      case 'threes':
        return this.getPredicatedSum(rolls, 3);
      case 'fours':
        return this.getPredicatedSum(rolls, 4);
      case 'fives':
        return this.getPredicatedSum(rolls, 5);
      case 'sixes':
        return this.getPredicatedSum(rolls, 6);
    }
  }

  getPointsForLowerSectionCombination(rolls, combination) {
    switch (combination) {
      case 'threeOfAKind':
        return this.validateCombination(rolls, combination) ? this.getCombinationSum(rolls) : 0;
      case 'fourOfAKind':
        return this.validateCombination(rolls, combination) ? this.getCombinationSum(rolls) : 0;
      case 'fullHouse':
        return this.validateCombination(rolls, combination) ? 25 : 0;
      case 'smallStraight':
        return this.validateCombination(rolls, combination) ? 30 : 0;
      case 'largeStraight':
        return this.validateCombination(rolls, combination) ? 40 : 0;
      case 'yahtzee':
        return this.validateCombination(rolls, combination) ? 50 : 0;
      case 'chance':
        return this.getCombinationSum(rolls);
      default:
        return 0;
    }
  }
}
